// const { rejects } = require('assert');
// const fs = require('fs');
// const { resolve } = require('path');


// const createDirectory = ((dir, path) => {
//     // fs.mkdir(dir, (err) => {
//     //     if (err) {
//     //         console.log(err);
//     //     }
//     //     fs.writeFileSync(path, JSON.stringify(path), "utf-8", (err) => {
//     //         if (err) {
//     //             console.log(err);
//     //         }
//     //     })
//     //     fs.unlink(path, (err) => {
//     //         if (err) {
//     //             console.log(err);
//     //         }
//     //     })
//     //     fs.rmdir(dir, (err) => {
//     //         if (err) {
//     //             console.log(err);
//     //         }
//     //     })
//     // })
//     return new Promise ((resolve, rejects) => {
//         fs.mkdir (dir, (err) => {
//             if (err) {
//                 rejects('Not Making the Directory');
//             }
//         })
//     })
// })

// module.exports = createDirectory;

const fs = require ('fs');

const makeDirectory = ((dir) => {
    return new Promise ((resolve, reject) => {
        fs.mkdir (dir, (err) => {
            if (err) {
                reject ('Not Found the file');
            }else {
                resolve ('SuccessFully Created Directory');
            }
        })
    })
})

const writeFile = ((path, data) => {
    return new Promise ((resolve, reject) => {
        fs.writeFile (path, data, (err) => {
            if (err) {
                reject ('Not write the file')
            }else {
                resolve ('Successfully Created the file.')
            }
        })
    })
})

const deleteFile = ((path) => {
    return new Promise ((resolve, reject) => {
        fs.unlink (path, (err) => {
            if (err) {
                reject ('Not deleting the file');
            }else {
                resolve ('Deleted FIle SuccessFully');
            }
        })
    })
})

const deleteDirectory = ((path) => {
    return new Promise ((resolve, reject) => {
        fs.rmdir (path, (err) => {
            if (err) {
                reject('Not Delete the directory');
            }else {
                resolve ('Successfully Delete the directory');
            }
        })
    })
})

module.exports = {makeDirectory, writeFile, deleteFile, deleteDirectory};