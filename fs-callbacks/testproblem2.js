const readFile = require ('./problem2.js');
const writeFile = require ('./problem2.js');
const appendFile = require ('./problem2.js');
const deleteFile = require ('./problem2.js');
const path = './../asynchronous-javascript/data/fs-callbacks/lipsum.txt';

readFile.readFile (path).then ((data) => {
    console.log(data);
    let newData = data.toUpperCase ();
    return writeFile.writeFile ('upper.txt', newData);
}).then((data) => {
    console.log(data);
    return writeFile.writeFile ('filenames.txt', 'upper.txt ');
}).then ((data) => {
    console.log(data);
    return readFile.readFile ('upper.txt');
}).then ((data) => {
    console.log(data);
    let newData = data.toLowerCase ();
    const result = newData.split ('.');
    return writeFile.writeFile ('lower.txt', result.join('\r\n'));
}).then ((data) => {
    console.log(data);
    return appendFile.appendFile ('filenames.txt', 'lower.txt ');
}).then ((data) => {
    console.log(data);
    return readFile.readFile ('lower.txt');
}).then((data) => {
    console.log(data);
    const newData = data.split (' ');
    const sortedData = newData.sort ();
    return writeFile.writeFile ('sorted.txt', sortedData.join());
}).then((data) => {
    console.log(data);
    return appendFile.appendFile ('filenames.txt', 'sorted.txt');
}).then((data) => {
    console.log(data);
    return readFile.readFile ('filenames.txt');
}).then ((data) => {
    console.log(data);
    return deleteFile.deleteFile ('upper.txt');
}).then ((data) => {
    console.log(data);
    return deleteFile.deleteFile ('lower.txt');
}).then ((data) => {
    console.log(data);
    return deleteFile.deleteFile ('sorted.txt');
}).then ((data) => {
    console.log(data);
}).catch ((err) => {
    console.log(err);
})