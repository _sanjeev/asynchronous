const fs = require ('fs');

const readFile = ((path) => {
    return new Promise ((resolve, reject) => {
        fs.readFile (path, 'utf-8', (err, data) => {
            if (err) {
                reject ('Not found the data');
            }else {
                resolve (data);
            }
        })
    })
})

const writeFile = ((path, filePath) => {
    return new Promise ((resolve, reject) => {
        fs.writeFile (path, JSON.stringify (filePath), 'utf-8', (err) => {
            if (err) {
                reject ('Not write the file');
            }else {
                resolve ('Successfully read the file');
            }
        })
    })
})

const appendFile = ((path, data) => {
    return new Promise ((resolve, reject) => {
        fs.appendFile (path, data, (err) => {
            if (err) {
                reject ('Not append the file');
            }else {
                resolve ('Successfully Append the file');
            }
        })
    })
})

const deleteFile = ((path) => {
    return new Promise ((resolve, reject) => {
        fs.unlink (path, (err) => {
            if (err) {
                reject ('Not delete the file');
            }else {
                resolve ('Successfully delete the file.');
            }
        })
    })
})

// const problem2 = ((filePath) => {
//     fs.readFile (filePath, "utf-8", (err, data) => {
//         if (err) {
//             console.log (err);
//         }else {
//             console.log (data);
//             fs.writeFileSync ('./upper.txt', data.toUpperCase(), 'utf-8', (err) => {
//                 if (err) {
//                     console.log (err);
//                 }
//             })
//             fs.writeFileSync ('./filenames.txt', 'upper.txt ', 'utf-8', (err) => {
//                 if (err) {
//                     console.log (err);
//                 }
//             })
//             fs.readFile ('./upper.txt', 'utf-8', (err, data) => {
//                 if (err) {
//                     console.log (err);
//                 }else {
//                     console.log (data);
//                     const lowerCase = data.toLowerCase();
//                     const sentense = lowerCase.split ('.');
//                     fs.writeFileSync ('./lowerSplit.txt', sentense.toString(), 'utf-8', (err) => {
//                         if (err) {
//                             console.log (err);
//                         }
//                     })

//                     fs.appendFile ('./filenames.txt', 'lowerSplit.txt ', (err) => {
//                         if (err) {
//                             console.log (err);
//                         }
//                     })
//                     fs.readFile ('./lowerSplit.txt', 'utf-8', (err, data) => {
//                         if (err) {
//                             console.log (err);
//                         }else {
//                             const newData = data.split (' ');
//                             fs.writeFileSync ('./sort.txt', newData.sort().toString(), (err, data) => {
//                                 if (err) {
//                                     console.log (err);
//                                 }else {
//                                     console.log (data);
//                                 }
//                             })
//                             fs.appendFile ('./filenames.txt', 'sort.txt ', (err) => {
//                                 if (err) {
//                                     console.log (err);
//                                 }
//                             })
//                             fs.readFile ('./filenames.txt', 'utf-8', (err, data) => {
//                                 if (err) {
//                                     console.log (err);
//                                 }else {
//                                     console.log (data);
//                                     const deleteFile = data.split (' ');
//                                     deleteFile.forEach ((fileName) => {
//                                         if (fs.existsSync (fileName)) {
//                                             fs.unlink (fileName, (err) => {
//                                                 if (err) {
//                                                     console.log (err);
//                                                 }
//                                             })
//                                         }
//                                     })
//                                 }
//                             })
//                         }
//                     })                    
//                 }
//             })
//         }
//     })
// })

module.exports = {writeFile, readFile, appendFile, deleteFile};