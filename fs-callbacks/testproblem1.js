// const createDirectory = require ('./problem1.js');
// const path = './Dir/random.json';
// const dir = './Dir'

// createDirectory (dir, path).then ((data) => {
//     console.log('Data');
// }).catch ((err) => {
//     return err;
// });

const makeDirectory = require ('./problem1.js');
const writeFile = require ('./problem1.js');
const deleteFile = require ('./problem1.js');
const deleteDirectory = require ('./problem1.js');

// import {makeDirectory, writeFile, deleteFile, deleteDirectory} from 'problem1.js'

makeDirectory.makeDirectory('./dir').then ((data) => {
    console.log(data);
    return writeFile.writeFile ('./dir/a.json', "greeting: Hello");
}).then ((data) => {
    console.log(data);
    return deleteFile.deleteFile ('./dir/a.json');
}).then ((data) => {
    console.log(data);
    return deleteDirectory.deleteDirectory ('./dir');
}).then ((data) => {
    console.log(data);
}).catch ((err) => {
    console.log(err);
});
