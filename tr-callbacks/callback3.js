const getAllCards = ((path, listId) => {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            const res = path[listId];
            if (res === undefined) {
                reject('Not find the data');
            } else {
                resolve(res);
            }
        }, 2 * 1000)
    })
})


module.exports = getAllCards;