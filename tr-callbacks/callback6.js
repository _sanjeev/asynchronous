const board = require('./../asynchronous-javascript/data/trello-callbacks/boards.json');
const list = require('./../asynchronous-javascript/data/trello-callbacks/lists.json');
const card = require('./../asynchronous-javascript/data/trello-callbacks/cards.json');
const getAllCards = require('./callback3.js');
const getAllList = require('./callback2.js');
const getBoardData = require('./callback1.js');


const getData = ((boardId) => {
    return new Promise((resolve, reject) => {
        setTimeout(async () => {

            try {
                const boardData = await getBoardData(board, boardId);
                console.log(boardData);
                const listData = await getAllList(list, boardId);
                console.log(listData);
                const listId = listData.map((key) => {
                    return key.id;
                })
                listData.forEach (async(val) => {
                    try {
                        const data = await getAllCards (card, val.id);
                        console.log(data);
                        // Promise.all ([getAllCards (card, val.id)]).then ((data) => {
                        //     console.log(data);
                        // }).catch ((err) => {
                        //     console.log(err);
                        // })
                    } catch (error) {
                        
                    }
                })

                // console.log(output);
                
            } catch (error) {
                console.log('cannot find the data');
            }

        }, 2 * 1000)
    })
})



module.exports = getData;