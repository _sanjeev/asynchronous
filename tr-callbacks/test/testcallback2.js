const getAllList = require("../callback2");

const path = require('./../../asynchronous-javascript/data/trello-callbacks/lists.json');

getAllList(path, 'mcu453ed').then ((data) => {
    console.log(data);
}).catch ((err) => {
    console.log(err);
});