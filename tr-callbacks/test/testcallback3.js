const getAllCards = require('./../callback3.js');
const path = require('./../../asynchronous-javascript/data/trello-callbacks/cards.json');

getAllCards(path, 'qwsa221').then ((data) => {
    console.log(data);
}).catch ((err) => {
    console.log(err);
});