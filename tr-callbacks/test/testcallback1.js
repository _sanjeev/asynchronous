const getBoardData = require("../callback1");

const path = require('./../../asynchronous-javascript/data/trello-callbacks/boards.json');

getBoardData(path, 'mcu453ed').then ((data) => {
    console.log(data);
}).catch ((err) => {
    console.log(err);
});