const fs = require('fs');

const getAllList = ((lists, boardId) => {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            const res = lists[boardId];
            if (res === undefined) {
                reject('Not found the data');
            } else {
                resolve(res);
            }
        }, 2 * 1000)
    })
})

module.exports = getAllList;