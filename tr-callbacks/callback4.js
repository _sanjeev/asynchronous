const getAllCards = require('./callback3.js');
const getAllList = require('./callback2.js');
const getBoardData = require('./callback1.js');
const board = require('./../asynchronous-javascript/data/trello-callbacks/boards.json');
const list = require('./../asynchronous-javascript/data/trello-callbacks/lists.json');
const card = require('./../asynchronous-javascript/data/trello-callbacks/cards.json');

const getData = ((boardId) => {
    return new Promise((resolve, reject) => {
        setTimeout(() => {

            // getBoardData (board, boardId).then ((data) => {
            //     console.log(data);
            // }).catch ((err) => {
            //     console.log(err);
            // });

            // getAllList (list, boardId).then ((data) => {
            //     console.log(data);
            // });
            
            const mindList = list[boardId];
            // getAllCards (card, mindList[0].id).then ((data) => {
            //     console.log(data);
            // }).catch ((err) => {
            //     console.log(err);
            // })

            Promise.all ([getBoardData (board, boardId), getAllList (list, boardId), getAllCards (card, mindList[0].id)]).then ((data) => {
                console.log(data);
            }).catch ((err) => {
                console.log(err);
            })
            
            
        }, 2 * 1000)
    })
})



module.exports = getData;