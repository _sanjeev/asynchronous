const fs = require('fs');
const getBoardData = ((path, boardId) => {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            const output = path.filter((val) => {
                if (val.id === boardId) {
                    return true;
                }
            })
            if (output === undefined) {
                reject('Not found the data');
            } else {
                resolve(output);
            }
        }, 2 * 1000)
    })
})



module.exports = getBoardData;