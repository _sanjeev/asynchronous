const board = require('./../asynchronous-javascript/data/trello-callbacks/boards.json');
const list = require('./../asynchronous-javascript/data/trello-callbacks/lists.json');
const card = require ('./../asynchronous-javascript/data/trello-callbacks/cards.json');
const getAllCards = require('./callback3.js');
const getAllList = require('./callback2.js');
const getBoardData = require('./callback1.js');

const getData = ((boardId) => {
    return new Promise((resolve, reject) => {
        setTimeout(() => {

            const mindList = list[boardId];
            // console.log(mindList[1].id);
            Promise.all ([getBoardData (board, boardId), getAllList (list, boardId), getAllCards (card, mindList[0].id), getAllCards (card, mindList[1].id)]).then ((data) => {
                resolve (data);
            }).catch ((err) => {
                reject ('Not found the data');
            })

        }, 2 * 1000)
    })
})

module.exports = getData;